from django.shortcuts import render, get_object_or_404
from django.utils import timezone
from .models import Post
from django.core.paginator import Paginator




def show_post(request):
    # posts = Post.objects.filter(publish_date__lte=timezone.now()).order_by('publish_date')

    post_list = Post.objects.all()
    paginator = Paginator(post_list, 4)  # Show 5 contacts per page.

    page_number = request.GET.get('page')
    page_obj = paginator.get_page(page_number)
    return render(request, 'index.html', {'posts': page_obj})


def post_page(request, slug):
    unique_slug = get_object_or_404(Post, slug=slug)
    return render(request, "page.html", {"post": unique_slug})

