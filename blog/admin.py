from django.contrib import admin
from .models import Post


@admin.register(Post)
class PostAdmin(admin.ModelAdmin):
    prepopulated_fields = {'slug': ('title',)}
    class Meta:
        model = Post


# admin.site.register(Post)
# Register your models here.
