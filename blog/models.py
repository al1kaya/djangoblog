from django.db import models




class Post(models.Model):
    title = models.CharField(max_length=100)
    article = models.TextField()
    publish_date = models.DateTimeField()
    excerpt = models.CharField(max_length=100)
    #featured_image = models.FileField(null=True, blank=True)
    slug = models.SlugField()

    def __str__(self):  # admin panelde "post object" yerine yazi isimlerini gösteren kod.
        return self.title
